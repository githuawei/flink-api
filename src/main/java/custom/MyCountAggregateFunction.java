package custom;

import beans.SensorReading;
import org.apache.flink.api.common.functions.AggregateFunction;

/**
 * 自定义计数聚合函数
 *
 * @author lvbingbing
 * @date 2022-01-01 13:05
 */
public class MyCountAggregateFunction implements AggregateFunction<SensorReading, Integer, Integer> {

    private static final long serialVersionUID = 283242033998316324L;

    @Override
    public Integer createAccumulator() {
        return 0;
    }

    @Override
    public Integer add(SensorReading sensorReading, Integer accumulator) {
        // 统计次数
        return accumulator + 1;
    }

    @Override
    public Integer getResult(Integer accumulator) {
        return accumulator;
    }

    @Override
    public Integer merge(Integer accumulator1, Integer accumulator2) {
        return accumulator1 + accumulator2;
    }
}
