package day02.environment;

import com.alibaba.fastjson.JSON;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.streaming.api.environment.LocalStreamEnvironment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Flink 流处理 API - environment
 *
 * @author lvbingbing
 * @date 2021-09-04 16:48
 */
public class FlinkEnvironment {
    public static void main(String[] args) {
        // 1、getExecutionEnvironment。创建一个执行环境，表示当前执行程序的上下文。最常用的一种创建执行环境的方式。
        // 如果程序是独立调用的，则此方法返回本地执行环境；如果从命令行客户端调用程序以提交到集群，则此方法返回此集群的执行环境
        // 1.1 批处理
        ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
        String s1 = JSON.toJSONString(env);
        System.out.println(s1);
        // 1.2 流处理环境
        StreamExecutionEnvironment streamEnv = StreamExecutionEnvironment.getExecutionEnvironment();
        String s2 = JSON.toJSONString(streamEnv);
        System.out.println(s2);

        // 2、createLocalEnvironment。返回本地执行环境。需要再调用时指定默认的并行度
        LocalStreamEnvironment localEnv = StreamExecutionEnvironment.createLocalEnvironment();
        String s3 = JSON.toJSONString(localEnv);
        System.out.println(s3);

        // 3、createRemoteEnvironment。返回集群执行环境。需要在调用时指定 JobManager的 IP 和端口号，并指定要在集群中运行的 Jar 包。
        String host = "hostname";
        int port = 6666;
        StreamExecutionEnvironment remoteEnv = StreamExecutionEnvironment.createRemoteEnvironment(host, port, "YOURPATH//WordCount.jar");
        String s4 = JSON.toJSONString(remoteEnv);
        System.out.println(s4);

    }
}
