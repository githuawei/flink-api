package day04;

import beans.SensorReading;
import custom.MyCountMapper;
import day03.window.FlinkWindow00;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Flink 状态管理 - 算子状态
 *
 * @author lvbingbing
 * @date 2022-01-05 00:42
 */
public class FlinkState01 {
    public static void main(String[] args) throws Exception {
        // 1、创建 FlinkWindow00 对象，有参构造会初始化 env，并从socket文本流中读取数据
        int parallelism = 1;
        FlinkWindow00 flinkWindow = new FlinkWindow00(parallelism);
        // 2、获取可执行环境
        StreamExecutionEnvironment env = flinkWindow.getEnv();
        // 3、学习算子状态
        studyOperatorState(flinkWindow.getSingleOutputStreamOperator());
        // 4、触发程序执行
        env.execute();
    }

    /**
     * 算子状态
     *
     * @param sensorReadingStream <br>
     */
    private static void studyOperatorState(SingleOutputStreamOperator<SensorReading> sensorReadingStream) {
        sensorReadingStream.map(new MyCountMapper())
                .print("operator state");
    }
}