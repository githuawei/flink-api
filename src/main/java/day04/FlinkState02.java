package day04;

import beans.SensorReading;
import custom.MyKeyCountMapper;
import day03.window.FlinkWindow00;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * Flink 状态管理 - 键控状态
 *
 * @author lvbingbing
 * @date 2022-01-05 22:26
 */
public class FlinkState02 {
    public static void main(String[] args) throws Exception {
        // 1、创建 FlinkWindow00 对象，有参构造会初始化 env，并从socket文本流中读取数据
        int parallelism = 1;
        FlinkWindow00 flinkWindow = new FlinkWindow00(parallelism);
        // 2、获取可执行环境
        StreamExecutionEnvironment env = flinkWindow.getEnv();
        // 3、学习键控状态
        studyKeyedState(flinkWindow.getSingleOutputStreamOperator());
        // 4、触发程序执行
        env.execute();
    }

    /**
     * 键控状态
     *
     * @param sensorReadingStream
     */
    private static void studyKeyedState(SingleOutputStreamOperator<SensorReading> sensorReadingStream) {
        sensorReadingStream.keyBy("id")
                .map(new MyKeyCountMapper())
                .print("keyedState");
    }
}