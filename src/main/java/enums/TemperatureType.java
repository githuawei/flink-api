package enums;

/**
 * @author lvbingbing
 * @date 2021-12-13 22:50
 */
public enum TemperatureType {

    HIGH("high"),
    LOW("low");

    public final String type;

    TemperatureType(String type) {
        this.type = type;
    }
}
